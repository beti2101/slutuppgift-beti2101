from cmd import Cmd
import time
from datetime import datetime
import jsons


class Bok:
    def __init__(self, titel_bok, forfattare, sidor, inkopsprisbok, inkopsårbok):
        self.titel = titel_bok
        self.forfattare = forfattare
        self.sidor = sidor
        self.inkopspris = inkopsprisbok
        self.inkopsår = inkopsårbok

    def __str__(self):
       return f"Titel: {self.titel}\nFörfattare: {self.forfattare}\nAntal sidor: {self.sidor}\nInköpspris: {self.inkopspris}\nInköpsår: {self.inkopsår}"
    
    def nuvarande_varde(self):
         nuvarande_ar = datetime.today().year
         inkops_ar = self.inkopsår
         varde = self.inkopspris
         if nuvarande_ar - inkops_ar > 50:
               while nuvarande_ar > inkops_ar:
                  varde = varde - varde * 1.08
                  inkops_ar = inkops_ar + 1
         else:
               while nuvarande_ar > inkops_ar:
                  varde = varde - varde * 0.1
                  inkops_ar = inkops_ar + 1
         return varde

class film:
    def __init__(self, titel_film, regissor, langd_film, inkopsPrisfilm, inkopsArfilm, slitning):
        self.titel = titel_film
        self.regissor = regissor
        self.langd = langd_film
        self.inkopsPris = inkopsPrisfilm
        self.inkopsAr = inkopsArfilm
        self.slitning = slitning

    def __str__(self):
           return f"Titel: {self.titel}\nRegissör: {self.regissor}\nLängd: {self.langd}\nInköpspris: {self.inkopsPris}\nInköpsår: {self.inkopsAr}"

    def nuvarande_varde(self):
        nuvarande_ar = datetime.today().year
        inkops_ar = self.inkopsAr
        varde = self.inkopsPris
        while nuvarande_ar > inkops_ar:
            varde = varde - varde * 0.1
            inkops_ar = inkops_ar + 1
        return varde * (self.slitning/10)


class cd:
    def __init__(self, titel_cd, artist, spar, langd_cd, inkopsPriscd):
        self.titel = titel_cd
        self.artist = artist
        self.spar = spar
        self.langd = langd_cd
        self.inkopsPris = inkopsPriscd

    def __str__(self):
        return f"Titel: {self.titel}\nArtist: {self.artist}\nAntal spår: {self.spar}\nLängd: {self.langd}\nInköpspris: {self.inkopsPris}"

    def nuvarande_varde(self, all_cds):
      matchande_skivor = 0
      for cd in all_cds:
         if cd.titel == self.titel and cd.artist == self.artist:
            matchande_skivor = matchande_skivor + 1
      return round(self.inkopsPris / matchande_skivor, 0)

class menyVal(Cmd):
   bok_lista = []
   film_lista = []
   cd_lista = []

   def __init__(self):
      super().__init__()

      self.load_items()

      self.cmdloop()
   
   print("\n=================MENY=====================")
   prompt = "1: Tryck 1 för REGISTRERA en Bok.\n"\
            "2: Tryck 2 för REGISTRERA en Film\n"\
            "3: Tryck 3 för REGISTRERA en Cd-skiva\n------------------------------------------\n"\
            "4: Tryck 4 för PRESENTERA alla böcker\n"\
            "5: Tryck 5 för PRESENTERA alla filmer\n"\
            "6: Tryck 6 för PRESENTERA alla cd-skivor\n"\
            "7: Tryck 7 för PRESENTERA all media\n------------------------------------------\n"\
            "8: Tryck 8 för Skriva ut sorterad lista\n------------------------------------------\n"\
            "9: Tryck 9 för Spara & Avsluta\n==========================================\n"\
            "Val:"

   def do_8(self,arg) :
      """sorterar listan"""
      sorted_books = sorted(menyVal.bok_lista, key=lambda Bok: Bok.titel)
      for bok in sorted_books:
         print(bok)
         print(f"Nuvarande värde: {bok.nuvarande_varde()}\n")
         
      sorted_films = sorted(menyVal.film_lista, key=lambda Film: Film.titel)
      for films in sorted_films:
         print(films)
         print(f"Nuvarande värde: {films.nuvarande_varde()}\n")

      sorted_cd = sorted(menyVal.cd_lista, key=lambda Cd: Cd.titel)
      for Cds in sorted_cd:
         print(Cds)
         print(f"Nuvarande värde: {Cds.nuvarande_varde(self.cd_lista)}\n")
      
   def do_1(self, arg):
      """registrera bok"""
      try:
         bok_name = input("Skrv in namnet på boken: ").capitalize()                
         bok_forfattare = input("Skriv in författaren: ").capitalize()
         sidor_bok = input("Skriv in antal sidor: ")
         inkopspris_bok = int(input("skriv in inköps priset: "))
         inkopsår_bok = int(input("Skriv in inköps år: "))
         x = Bok(bok_name, bok_forfattare,sidor_bok,inkopspris_bok,inkopsår_bok)
         menyVal.bok_lista.append(x)
         print("Boken tillagd i biblioteket.......")
      except ValueError:
         print("\nDu glömde att ange ett värde.Försök igen!\n")   
      
   def do_2(self, arg):
      """Registrera film """
      try:
         film_name = input("Skriv in filmens namn: ").capitalize()
         film_regissor = input("Skriv in filmens regissör: ").capitalize()
         film_langd = input("Skriv in filmen längd: ")
         film_inkopspris = int(input("Skriv in filmens pris: "))
         film_inkopsår = int(input("Skriv in inköps år : "))
         film_slitning = int(input("Ange slitningsgrad(1-10): "))
         y = film(film_name,film_regissor,film_langd,film_inkopspris,film_inkopsår,film_slitning)
         menyVal.film_lista.append(y)
         print("Filmen tillagd i biblioteket.....")
      except ValueError:
         print("\nDu glömde att ange ett värde.Försök igen!\n")

   def do_3(self, arg):
      """Registrera cd"""
      try:
         cd_name = input("Ange cd-skivans namn: ").capitalize()
         cd_artist = input("Ange artisten: ").capitalize()
         cd_spar = int(input("Ange antal spår skivan har: "))
         cd_langd = input("Ange längd: ")
         cd_inkopspris = int(input("Ange inköps priset på skivan: "))
         z = cd(cd_name,cd_artist,cd_spar,cd_langd,cd_inkopspris,)
         menyVal.cd_lista.append(z)
         print("Skivan tillagd i bibliotekt.....")
      except ValueError:
         print("\nDu glömde att ange ett värde.Försök igen!\n")

   def do_4(self, arg):
      """Presentera bok"""
      for boks in menyVal.bok_lista:
         print(boks)
         print(f"Nuvarande värde: {round(boks.nuvarande_varde(),2)}\n")
  
   def do_5(self, arg):
      """Presentera film"""
      for film in menyVal.film_lista:
         print(film)
         print(f"Nuvarande värde: {round(film.nuvarande_varde(),2)}\n")

   def do_6(self, arg):
      """Presentera cd"""
      for cd in menyVal.cd_lista: 
         print(cd)
         print(f"Nuvarande värde: {cd.nuvarande_varde(self.cd_lista)}\n")

   def do_7(self,arg):
      """Presentera all media"""
      print("\nSKRIVER UT ALLA BÖCKER.....\n")
      for boks in menyVal.bok_lista:
         print(boks)
         print(f"Nuvarande värde: {round(boks.nuvarande_varde(),2)}\n")

      print("\nSKRIVER UT ALLA FILMER....\n ") 
      for film in menyVal.film_lista:
         print(film)
         print(f"Nuvarande värde: {round(film.nuvarande_varde(),2)}\n")

      print("\nSKRIVER UT ALLA CD-SKIVOR.....\n")
      for cd in menyVal.cd_lista:
         print(cd)
         print(f"Nuvarande värde: {cd.nuvarande_varde(self.cd_lista)}\n")

   def do_9(self, arg):
      """Spara och avsluta programmet"""
      self.store_items()
      print("Programmet avslutas...")
      time.sleep(1)
      return True

   def store_items(self):
      data = {
         "cds": self.cd_lista,
         "books": self.bok_lista,
         "movies": self.film_lista
      }
      with open("databas.json", "w") as f:
         f.write(jsons.dumps(data))

   def load_items(self):
      try:
         data = jsons.loads(open("databas.json", "r").read())
         for item in data['cds']:
            self.cd_lista.append(cd(item['titel'], item['artist'], item['spar'], item['langd'], item['inkopsPris']))
         for item in data['books']:
            self.bok_lista.append(Bok(item['titel'], item['forfattare'], item['sidor'], item['inkopspris'], item['inkopsår']))
         for item in data['movies']:
            self.film_lista.append(film(item['titel'], item['regissor'], item['langd'], item['inkopsPris'], item['inkopsAr'], item['slitning']))

      except Exception as e:
         print(f"Exception occured: {e}")

if __name__ == '__main__':
  menu =  menyVal()