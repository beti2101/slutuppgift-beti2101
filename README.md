
# Slutuppgift 
Uppgiften går ut på att man ska skapa ett bibliotek som man kan spara olika media i tex böcker, cdskivor och filmer.
Man ska mata in olika värden för olika medietyper och sedan kunna skriva ut all info.
När man avslutar programmet ska den spara all info i en json fil och sedan öppna json filen vid start av programmet.
#

# Krav för köra programmet
    Python verision 3.9 eller senare
    jsons
    

# Guide
 Se till att du har senaste verisionen av python och att du har installerat jsons(pip install jsons i terminalen).
 Starta programmet så får du fram en meny och välj sedan vad du vill registrera bok,film eller cd.
 Efter du har registrerat alla värden kan du antingen presentera alla böcker,filmer o cdskivor var för sig eller trycka meny val 7 för presentera all media eller meny val 8 för en sorterad lista.
 När du väljer att avsluta programmet så sparas all inmatad data i en jsons fil.
 Nästa gång du väljer att köra igång programmet så läser den in jsons filen igen.
#
Skapad av Berhun tiftikci nackademin DevOps 21